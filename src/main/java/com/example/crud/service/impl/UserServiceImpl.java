package com.example.crud.service.impl;

import java.util.List;
import java.util.Optional;

import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.crud.entity.User;
import com.example.crud.repo.UserRepo;
import com.example.crud.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepo userRepo;

	@Override
	public User create(User user) {
		return userRepo.save(user);
	}

	@Override
	public Optional<User> findUserById(int id) {
		return userRepo.findById(id);
	}

	@Override
	public List<User> getAll() {
		return userRepo.findAll();
	}

	@Override
	public Optional<User> deleteUserById(int id) {
		userRepo.deleteById(id);
		System.out.println("deleted");
		return null;
	}

	@Override
	public List<User> deleteAll() {
		userRepo.deleteAll();
		return null;

	}

	@Override
	public List<User> findUserByName(String name) {
		return userRepo.findByName(name);
	}

	@Override
	public List<User> findByNameAndEmail(String name, String email) {
		return userRepo.findByNameAndEmail(name,email);
	}

}
