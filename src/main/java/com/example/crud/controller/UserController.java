package com.example.crud.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.crud.entity.User;
import com.example.crud.service.UserService;
@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserService userService;
	
//	public ResponseEntity<User> save(){
//		return ResponseEntity.status(HttpStatus.OK).body(userService.save());
//	}
	@PostMapping("/")
public ResponseEntity<User> create(@RequestBody User user){
	return ResponseEntity.status(HttpStatus.CREATED).body(userService.create(user));

}
	@GetMapping("/")
	public ResponseEntity<Optional<User>> get(@RequestParam int id){
		return ResponseEntity.status(HttpStatus.OK).body(userService.findUserById(id));

	}
	@GetMapping("/allUsers")
	public ResponseEntity<List<User>> getAll(){
		return ResponseEntity.status(HttpStatus.OK).body(userService.getAll());

	}
	@DeleteMapping("/")
	public ResponseEntity<Optional<User>> deleteUser(@RequestParam int id) {
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.deleteUserById(id));

	}
	@DeleteMapping("/deleteAllusers")
	public ResponseEntity<List<User>> deleteAllUsers() {
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.deleteAll());

	}
	@GetMapping("/getUserByName")
	public ResponseEntity<List<User>> findUserByName(@RequestParam String name){
		return ResponseEntity.status(HttpStatus.OK).body(userService.findUserByName(name));

	}
	@GetMapping("/getUserByNameAndEmail")
	public ResponseEntity<List<User>> findUserByNameAndEmail(@RequestParam String name,String email){
		return ResponseEntity.status(HttpStatus.OK).body(userService.findByNameAndEmail(name,email));

	}

}
