package com.example.crud.service;

import java.util.List;
import java.util.Optional;

import com.example.crud.entity.User;

public interface UserService {
	User create(User user);

	Optional<User> findUserById(int id);

	List<User> getAll();

	Optional<User> deleteUserById(int id);
	
	List<User> deleteAll();
	List<User> findUserByName(String name);
	List<User> findByNameAndEmail(String name,String email);

}
