package com.example.crud.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.crud.entity.User;

public interface UserRepo extends JpaRepository<User, Integer>{

Optional<User> findById(User user);
List<User> findByName(String name);
List<User> findByNameAndEmail(String name, String email);
}
